# frozen_string_literal: true

class WordAndEmotion < ActiveRecord::Base
  validates :sentiment, numericality: {greater_than_or_equal_to: -10, less_than_or_equal_to: 10}
end