require_relative '../test_helper'

class WordAndEmotionTest < Minitest::Test
  def test_valid
    w = WordAndEmotion.new(word: 'самолет', sentiment: 5)
    assert w.valid?
  end

  def test_invalid
    w = WordAndEmotion.new(word: 'самолет', sentiment: 15)
    assert w.invalid?
  end
end