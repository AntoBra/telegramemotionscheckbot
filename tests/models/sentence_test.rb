require_relative '../../sentence.rb'
require_relative '../../mainfile.rb'
require 'minitest/autorun'

class TestApplication < Minitest::Test
  def setup
    @app = Application.new 'Some Sentence'
  end

  def test_avg_emotionality_not_nil
    assert !@app.average_emotionality.nil?
  end
end



# puts Sentence.new("Мама мыла нашу раму, хорошо она ее мыла").slice
# puts "\n"
# puts Sentence.new("Игорь23 прогомизд лол хех: тестируй грит прило$жение норм те%ма").slice