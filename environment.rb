require 'active_record'
ActiveRecord::Base.establish_connection(adapter: 'sqlite3', database: 'dbfile.sqlite3')

Dir.glob('./models/*.rb').each { |f| require_relative f }

