require_relative 'sentence.rb'
require_relative 'word.rb'

class Application
	def initialize(sentence)
    @sliced_sentence = Sentence.new(sentence).slice
	end

	def emotionality
    @full_emotionality ||= @sliced_sentence.each_with_object(0) do |i, sum|
      sum += Word.new(i).sentiment
    end
  end

  def average_emotionality
    emotionality/@sliced_sentence.length
  end
end

