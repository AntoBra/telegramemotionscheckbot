require_relative 'environment'

class Word
	REGULAR_WORDS = /[^а-яА-Я]/
	def initialize(word)
		@word = word
	end

	def sentiment
		@word.downcase.gsub(REGULAR_WORDS,'')
		::WordAndEmotion.where(:word=>@word).first&.sentiment.to_f
	end

end