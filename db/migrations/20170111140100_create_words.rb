class CreateWords < ActiveRecord::Migration[5.0]
  def change
    create_table :word_and_emotions do |t|
      t.string  :word
      t.numeric :sentiment, :default => 0
    end
  end
end