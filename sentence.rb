class Sentence
	def initialize(sentence)
		@sentence = sentence
	end

	def slice
		@sentence.downcase.split(/[[:space:]]/)
	end
end